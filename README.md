Docker HAProxy
==============

This Docker container is a HAProxy reverse proxy that automatically configures depending on Docker links. It is assumed that all the containers are subdomains of the same `$DOMAIN`, and that the link name is the subdomain. HTTP and HTTPS are managed, and if the linked container doesn't provide HTTPS, the encapsulation is performed by HAProxy.

Certificates are generated (self signed) in the volume `/etc/ssl/web` with the following names:

- `FQDN.cfg`: Certificare request OpenSSL configuration
- `FQDN.csr`: Certificate request
- `FQDN.key`: Private key
- `FQDN.crt`: Certificate, self signed if not provided
- `FQDN.pem`: Generated concatenation of the Certificare and the private key

The following environment variables are used:

- `DOMAIN`: used to fill the domain name.
- `HOST_<name>_PORT_<number>_DISABLE=true`: Disable the port `<number>` on the linked container `<name>`. It can be useful to mask the 443 port of a container to use HAProxy encapsulation instead.
- Docker generated variables for each container link are also used

Linked containers are assumed to listen on port 80 or 8080 for http and 443 for https.

