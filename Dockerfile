FROM haproxy
RUN apt-get update; DEBIAN_FRONTEND=noninteractive apt-get install -y openssl; apt-get clean
COPY entry.sh /entry.sh
EXPOSE 80 443
VOLUME ["/etc/ssl/web"]
ENTRYPOINT ["/bin/bash", "/entry.sh"]
CMD ["haproxy", "-f", "/usr/local/etc/haproxy/haproxy.cfg", "-db", "-dV"]
