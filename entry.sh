#!/bin/bash
# vim: sts=4:sw=4:et

hosts(){
    env | cut -d= -f1 | sed -n 's/_NAME$//p' | tr A-Z a-z
}

upper(){
    tr a-z A-Z <<<"$1"
}

lower(){
    tr A-Z a-z <<<"$1"
}

ipaddr(){
    local hostname="$1"
    sed -rn "s/^(\S*)\s+$hostname(\s.*)?\$/\1/p" /etc/hosts | head -n 1
}

foreach(){
    while read host; do
        "$@" "$host"
    done
}

has_port(){
    local host="$1"
    local port="$2"
    local varname="$(upper "$host")_PORT_${port}_TCP_PORT"
    local varname_disable="$(upper "$host")_PORT_${port}_DISABLE"
    if [[ -n "${!varname}" ]] && [[ "true" != "${!varname_disable}" ]]; then
        return 0
    else
        return 1
    fi
}

filter_http(){
    local host="$1"
    if has_port "$host" 80 ||  has_port "$host" 8080; then
        echo "$host"
    fi
}

filter_https(){
    local host="$1"
    if has_port "$host" 443; then
        echo "$host"
    fi
}

filter_non_https(){
    local host="$1"
    if ! has_port "$host" 443 && (has_port "$host" 80 || has_port "$host"  8080); then
        echo "$host"
    fi
}

httpport(){
    if has_port 8080; then
        echo 8080
    else
        echo 80
    fi

}

portnum(){
    echo $((500 + $(hosts | grep "^$1\$" | cut -d: -f1) ))
}

join(){
    while read line; do
        printf "%s${1:- }" "$line"
    done
}

generate_cert(){
    local dir="$1"
    local host="$2"
    local base="$dir/${host}.${DOMAIN}"
    if ! [[ -e "${base}.cfg" ]]; then
        cat >"${base}.cfg" <<EOF
[ req ]
prompt             = no
default_bits       = 2048
distinguished_name = req_distinguished_name

[ req_distinguished_name ]
CN = ${host}.${DOMAIN}
EOF
        [[ -e "${base}.key" ]] && touch -r "${base}.key" "${base}.cfg"
    fi
    if [[ "${base}.cfg" -nt "${base}.key" ]] || ! [[ -e "${base}.key" ]]; then
        echo "$host: Generating private key and certificate request"
        openssl req -config "${base}.cfg" -new -nodes \
            -keyout "${base}.key" -out "${base}.csr"
    fi
    if ! [[ -e "${base}.crt" ]]; then
        echo "$host: Generating self-signed certificate"
        openssl x509 -req -days 3650 -sha256 -in "${base}.csr" -signkey "${base}.key" -out "${base}.crt"
        echo "$host: SSL certificate $(openssl x509 -in "${base}.crt" -noout -fingerprint)"
    fi
    if ! [[ -e "${base}.dhparams" ]]; then
        openssl dhparam -out "${base}.dhparams" 2048
    fi
}

generate_pem(){
    local dir="$1"
    local host="$2"
    local base="$dir/${host}.${DOMAIN}"
    if ! [[ -e "${base}.pem" ]] || [[ "${base}.crt" -nt "${base}.pem" ]] || [[ "${base}.key" -nt "${base}.pem" ]] || [[ "${base}.dhparams" -nt "${base}.pem" ]]; then
        echo "$host: Concatenate key and certificate in .pem file"
        cat "${base}.crt" "${base}.dhparams" "${base}.key" > "${base}.pem"
    fi
}

#########################

mkdir -p /etc/ssl/web

hosts | foreach filter_non_https | foreach generate_cert /etc/ssl/web
hosts | foreach generate_pem /etc/ssl/web

#########################

host_acl(){
    printf "\tacl http_host_$1 hdr(host) -i $1.$DOMAIN\n"
}

sni_acl(){
    printf "\tacl https_host_$1 req_ssl_sni -i $1.$DOMAIN\n"
}

use_backend(){
    local proto="$1"
    local host="$2"
    printf "\tuse_backend ${proto}_servers_${host} if ${proto}_host_${host}\n"
}

use_backend_convert(){
    local proto="https"
    local host="$1"
    printf "\tuse_backend ${proto}_convert_${host} if ${proto}_host_${host}\n"
}

backend_https_convert(){
    local host="$1"
    printf "backend https_convert_${host}\n"
    printf "\tmode tcp\n"
    printf "\tserver https_convert_server_${host} 127.0.0.1:$(portnum "$host")\n"
}

frontend_https_convert(){
    local host="$1"
    printf "frontend https_front_$host\n"
    printf "\tbind 127.0.0.1:$(portnum "$host") ssl crt /etc/ssl/web/${host}.${DOMAIN}.pem\n"
    printf "\tmode http\n"
    printf "\tdefault_backend http_servers_$host\n"
    printf "\trspadd Strict-Transport-Security:\\ max-age=31536000\n"
    #printf "\trspadd X-Frame-Options:\\ SAMEORIGIN\n"
}

default_host_backend(){
    if [[ -n "$DEFAULT_HOST" ]]; then
        printf "\tdefault_backend ${1}_servers_$DEFAULT_HOST\n"
    fi
}

backend_servers(){
    local proto="$1"
    local host="$2"
    printf "backend ${proto}_servers_${host}\n"
    printf "\tserver ${proto}_server_${host}_1 $(ipaddr "$host"):$(httpport "$host")\n"
}

backend_https_servers(){
    local proto="https"
    local host="$1"
    printf "backend ${proto}_servers_${host}\n"
    printf "\tmode tcp\n"
    printf "\tserver ${proto}_server_${host}_1 $(ipaddr "$host"):443\n"
}


# host listen to http and https
# http -> redirect to backend via host
# https -> redirect to backend via sni

# host listen to http only
# http -> redirect to backend via host
# https -> redirect to http-convert via sni
# http-convert -> redirect to backend via host

# host listen to https only
# http -> none
# https -> redirect to backend via sni

(
cat <<EOF

global
        daemon
        maxconn ${MAXCONN:-256}
        ssl-default-bind-options no-sslv3 no-tls-tickets force-tlsv12
        ssl-default-bind-ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK
        # https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_configurations

defaults
        mode http
        timeout connect ${TIMEOUT_CONNECT:-5000ms}
        timeout client ${TIMEOUT_CLIENT:-50000ms}
        timeout server ${TIMEOUT_SERVER:-50000ms}

frontend http
        bind *:80

        # ACL
$(hosts | foreach filter_http | foreach host_acl)

        # Backends
$(hosts | foreach filter_http | foreach use_backend http)
$(default_host_backend http)

frontend https
        bind *:443
        mode tcp

        tcp-request inspect-delay 5s
        tcp-request content accept if { req_ssl_hello_type 1 } 

        # ACL
$(hosts | foreach sni_acl)

        # Backends
$(hosts | foreach filter_https | foreach use_backend https)
$(hosts | foreach filter_non_https | foreach use_backend_convert)
$(default_host_backend https)

#
# HTTPS Backends
#

$(hosts | foreach filter_https | foreach backend_https_servers)

#
# HTTP to HTTPS Backends
# $(hosts | foreach filter_non_https | join)
#

$(hosts | foreach filter_non_https | foreach backend_https_convert)

#
# HTTP to HTTPS Frontends
# $(hosts | foreach filter_https | join)
#

$(hosts | foreach filter_non_https | foreach frontend_https_convert)

#
# HTTP Backends
# $(hosts | foreach filter_http | join)
#

$(hosts | foreach filter_http | foreach backend_servers http)

EOF

) >/usr/local/etc/haproxy/haproxy.cfg

cat /usr/local/etc/haproxy/haproxy.cfg >&2 
set -x
exec "$@"

